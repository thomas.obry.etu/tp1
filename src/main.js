const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];
function compName(a,b){
	if(a.name < b.name)
		return -1;
	if(a.name > b.name)
		return 1;
	return 0;
}
function compPriceLarge(a,b){
	if(a.price_large < b.price_large)
		return -1;
	if(a.price_large > b.price_large)
		return 1;
	return 0; 
}
function compPrice(a,b){
	if(a.price_small < b.price_small)
		return -1;
	if(a.price_small > b.price_small)
		return 1;
	return compPriceLarge(a,b);
}

data.sort(compPrice);
const newtab = data.filter(a => a.name.split('i').length - 1  == 2);

// const newtab = data.map(mapper);
// function mapper({image,name,price_small,price_large}){
// 	return `<article class="pizzaThumbnail">
// 		<a href="${image}">
// 			<img src="${image}" />
// 			<section>
// 				<h4>${name}</h4>
// 				<ul>
// 					<li>Prix petit format : ${price_small.toFixed(2)} €</li>
// 					<li>Prix grand format : ${price_large.toFixed(2)} €</li>
// 				</ul>
// 			</section>
// 		</a>
// 	</article>`;
// }
// console.log(newtab);
// document.querySelector('.pageContent').innerHTML = newtab.join(' ');

const reducer = (accumulator, {image,name,price_small,price_large}) => accumulator +
`<article class="pizzaThumbnail">
	<a href="${image}">
		<img src="${image}" />
		<section>
			<h4>${name}</h4>
			<ul>
                <li>Prix petit format : ${price_small.toFixed(2)} €</li>
				<li>Prix grand format : ${price_large.toFixed(2)} €</li>
			</ul>
		</section>
	</a>
</article>`;

document.querySelector('.pageContent').innerHTML = data.reduce(reducer, '');